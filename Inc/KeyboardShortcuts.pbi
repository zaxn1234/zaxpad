﻿; ZaxPad Keyboard Shortcuts

; File
AddKeyboardShortcut(#Window_Main, #PB_Shortcut_Control | #PB_Shortcut_N, #Menu_File_New)
AddKeyboardShortcut(#Window_Main, #PB_Shortcut_Control | #PB_Shortcut_S, #Menu_File_Save)
AddKeyboardShortcut(#Window_Main, #PB_Shortcut_Control | #PB_Shortcut_Shift | #PB_Shortcut_S, #Menu_File_SaveAs)
AddKeyboardShortcut(#Window_Main, #PB_Shortcut_Control | #PB_Shortcut_W, #Menu_File_CloseTab)
AddKeyboardShortcut(#Window_Main, #PB_Shortcut_Control | #PB_Shortcut_O, #Menu_File_Open)
AddKeyboardShortcut(#Window_Main, #PB_Shortcut_Control | #PB_Shortcut_Shift | #PB_Shortcut_O, #Menu_File_OpenFolder)

; Edit

; Help
AddKeyboardShortcut(#Window_Main, #PB_Shortcut_F1, #Menu_Help_Help)
AddKeyboardShortcut(#Window_Main, #PB_Shortcut_Control | #PB_Shortcut_Alt | #PB_Shortcut_F1, #Menu_Help_About)
; IDE Options = PureBasic 5.72 (Windows - x64)
; CursorPosition = 10
; EnableXP