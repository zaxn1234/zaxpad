﻿; ZaxPad Menu
CreateMenu(#Menu_Main, WindowID(#Window_Main))
  MenuTitle("File")
    MenuItem(#Menu_File_New, "&New File" + Chr(9) + "Ctrl + N") : DisableMenuItem(#Menu_Main, #Menu_File_New, #True)
    MenuItem(#Menu_File_Open, "&Open File" + Chr(9) + "Ctrl + O")
    MenuItem(#Menu_File_OpenFolder, "Open &Folder" + Chr(9) + "Ctrl + Shift + O")
    MenuItem(#Menu_File_Save, "&Save File" + Chr(9) + "Ctrl + S") : DisableMenuItem(#Menu_Main, #Menu_File_Save, #True)
    MenuItem(#Menu_File_SaveAs, "Save File &As" + Chr(9) + "Ctrl + Shift + S") : DisableMenuItem(#Menu_Main, #Menu_File_SaveAs, #True)
    MenuItem(#Menu_File_CloseTab, "Close &Tab" + Chr(9) + "Ctrl + w")
    MenuBar()
    MenuItem(#Menu_File_Exit, "E&xit" + Chr(9) + "Alt + F4")
  MenuTitle("Edit")
  MenuTitle("Help")
    MenuItem(#Menu_Help_Help, "Help" + Chr(9) + "F1") : DisableMenuItem(#Menu_Main, #Menu_Help_Help, #True)
    MenuBar()
    MenuItem(#Menu_Help_About, "About" + Chr(9) + "Ctrl + Alt + F1")
; IDE Options = PureBasic 5.72 (Windows - x64)
; CursorPosition = 15
; EnableXP