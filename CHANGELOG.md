﻿# ZaxPad Changelog

## 0.0.1 - 18/08/2021

### Added
- Added new tab system
- Basic `About` window
- Added keyboard shortcuts

### Changed
- Main theme is now a dark theme
- Tree gadget is now explorer gadget
- Explorer gadget hidden on initial program load and only displayed when folder opened
- Status bar now shows current file path in first field

### Fixed
- Line/Char numbering in status bar is now correct

## 0.0.0 - Current Repo Version (12/08/2021)
Initial Dev Commit
