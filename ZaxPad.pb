﻿;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;                                                ;
;                     ZaxPad                     ;
;                                                ;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

EnableExplicit

;- Enumeration
Enumeration
  ;-> Window
  #Window_Main
  #Window_About
  
  ;-> File
  #File_Open
  
  ;-> Panel
  #Panel_Main
  
  ;-> List
  #Tree_Files
  
  ;-> Statusbar
  #StatusBar
  
  ;-> Menu
  #Menu_Main
  
  #Menu_File_New
  #Menu_File_Open
  #Menu_File_OpenFolder
  #Menu_File_Save
  #Menu_File_SaveAs
  #Menu_File_CloseTab
  #Menu_File_Exit
  
  #Menu_Help_Help
  #Menu_Help_About
  
  ;-> Buttons
  #Button_About_Close
  
  ;-> Text
  #Text_About
  #Text_About_Author
  #Text_About_Version
  
  ;-> Editor
  #Editor_Main = 100
   
EndEnumeration  

;- Global Declarations
;-> Constants
#APP_NAME = "ZaxPad"
#APP_VERSION_MAJOR = 0
#APP_VERSION_MINOR = 0
#APP_VERSION_PATCH = 1
;-> Variables
Global APP_TITLE$ = #APP_NAME + " v" + Str(#APP_VERSION_MAJOR) + "." + Str(#APP_VERSION_MINOR) + "." + Str(#APP_VERSION_PATCH)
Global Quit = 0, Event
Global DarkThemeColour = RGB(41, 45, 62)
Global TenPCWidth, SBHeight
;-> Procedures
Declare Inits()
Declare LoadUI()
Declare UpdateStatusBar()
Declare Exit()
Declare ScintillaCallback(Gadget, *scinotify.SCNotification)
Declare OpenEditorFile(FileName$="")
Declare OpenEditorFolder()
Declare CreateScintillaEditor(TabIndex.i)
Declare CloseTab(TabIndex.i)
Declare About()
Declare AboutClose()

;- Procedure List
Procedure Inits()
  Protected res
  InitScintilla()  
  
  LoadUI()
  
  XIncludeFile "Inc/KeyboardShortcuts.pbi"
EndProcedure

Procedure LoadUI()
  If OpenWindow(#Window_Main, 10, 10, 800, 600, APP_TITLE$, #PB_Window_ScreenCentered | #PB_Window_Maximize | #PB_Window_MinimizeGadget | #PB_Window_SystemMenu) = 0
    MessageRequester("Error", "Cannot open the main window!", #MB_OK)
    End
  EndIf
  
  ; Everything opened correctly
  SetWindowColor(#Window_Main, DarkThemeColour)
  TenPCWidth = WindowWidth(#Window_Main) / 10
    
  ; StatusBar
  CreateStatusBar(#StatusBar, WindowID(#Window_Main))
  StatusBarHeight(#StatusBar)
  AddStatusBarField(TenPCWidth * 2)
  AddStatusBarField(TenPCWidth * 5)
  AddStatusBarField(TenPCWidth * 2)
  AddStatusBarField(TenPCWidth)
  
  SBHeight = StatusBarHeight(#StatusBar)
  
  ; Create Menu
  XIncludeFile "Inc/Menu.pbi"
       
  ; Create the gadgets
  ; List of files
  ExplorerTreeGadget(#Tree_Files, 0, 0, TenPCWidth, WindowHeight(#Window_Main) - SBHeight - 20, "", #PB_Explorer_NoLines | #PB_Explorer_NoParentFolder | #PB_Explorer_NoMyDocuments)
  SetGadgetColor(#Tree_Files, #PB_Gadget_BackColor, DarkThemeColour)  
  SetGadgetColor(#Tree_Files, #PB_Gadget_FrontColor, RGB(255, 255, 255))  
  HideGadget(#Tree_Files, 1)
  
  ; Editor
  PanelGadget(#Panel_Main, TenPCWidth, 0, TenPCWidth * 9, WindowHeight(#Window_Main) - SBHeight - 18)
  AddGadgetItem(#Panel_Main, -1, "New File")
  
  CreateScintillaEditor(0)
   
  CloseGadgetList()
   
  ; Set status bar text
  UpdateStatusBar()
EndProcedure

Procedure CreateScintillaEditor(TabIndex.i)
  Protected NewEnum
  NewEnum = #Editor_Main + TabIndex
  
  ScintillaGadget(NewEnum, 0, 0, TenPCWidth * 9, WindowHeight(#Window_Main) - SBHeight - 44, @ScintillaCallback())
  ScintillaSendMessage(NewEnum, #SCI_STYLESETFORE, 0, RGB(255, 255, 255))
  ScintillaSendMessage(NewEnum, #SCI_STYLESETBACK, 0, DarkThemeColour) 
  ScintillaSendMessage(NewEnum, #SCI_STYLESETBACK, 32, DarkThemeColour) 
  ScintillaSendMessage(NewEnum, #SCI_STYLESETBACK, 33, RGB(0, 0, 0)) 
  ScintillaSendMessage(NewEnum, #SCI_GETFOCUS)
  ScintillaSendMessage(NewEnum, #SCI_SETWRAPMODE, 1) 
  ScintillaSendMessage(NewEnum, #SCI_SETWRAPVISUALFLAGS, 4)
  ScintillaSendMessage(NewEnum, #SCI_STYLESETSIZE, #STYLE_DEFAULT, 16)
  
  ProcedureReturn NewEnum
EndProcedure

Procedure CloseTab(TabIndex.i)
  Protected Resp

  If ScintillaSendMessage(#Editor_Main + TabIndex, #SCI_GETMODIFY) <> 0
    ; Modified File
    Resp = MessageRequester("File Modified", "Do you want to save your changes before quitting?", #PB_MessageRequester_YesNo)
    If Resp = #PB_MessageRequester_Yes
       ; @TODO: Save File
    EndIf
  EndIf
 
  ; Remove Scintilla Gadget
  If TabIndex <> 0 : TabIndex + 1 : EndIf
  If IsGadget(#Editor_Main + TabIndex)
    FreeGadget(#Editor_Main + TabIndex)
  EndIf
  
  ; Remove Panel
  If TabIndex <> 0 : TabIndex - 1 : EndIf
  RemoveGadgetItem(#Panel_Main, TabIndex)    
  
  If CountGadgetItems(#Panel_Main) = 0
    ; No tabs open, default to 'new' tab
    OpenGadgetList(#Panel_Main)
      AddGadgetItem(#Panel_Main, -1, "New File")
      CreateScintillaEditor(0)
    CloseGadgetList()
  EndIf
EndProcedure

Procedure UpdateStatusBar()
  Protected LineNum, CharNum
  
  ; Current File
  StatusBarText(#StatusBar, 0, GetGadgetItemText(#Panel_Main, GetGadgetState(#Panel_Main)))
  
  ; Last Saved Status?
  
  ; Language/Lexer used
  
  ; Char/Line details
  LineNum = ScintillaSendMessage(#Editor_Main, #SCI_LINEFROMPOSITION, ScintillaSendMessage(#Editor_Main, #SCI_GETCURRENTPOS)) + 1
  CharNum = ScintillaSendMessage(#Editor_Main, #SCI_GETCURLINE)
  StatusBarText(#StatusBar, 3, "Line: " + LineNum  + " | Chr: " + CharNum)
  
EndProcedure

Procedure About()
  
  OpenWindow(#Window_About, 0, 0, 400, 300, "About " + #APP_NAME, #PB_Window_ScreenCentered)
  StickyWindow(#Window_About, #True)
  
  ; @TODO: Add Logo and better descriptive text
  
  ButtonGadget(#Button_About_Close, 330, 250, 60, 40, "Close")
  TextGadget(#Text_About, 10, 10, 380, 200, #APP_NAME + " is a custom IDE built with versatility in mind.")
  TextGadget(#Text_About_Version, 10, 255, 300, 20, "Version " + #APP_VERSION_MAJOR + "." + #APP_VERSION_MINOR + "." + #APP_VERSION_PATCH)
  TextGadget(#Text_About_Author, 10, 275, 300, 30, "Created by Izaak Webster")
EndProcedure

Procedure AboutClose()
  
  CloseWindow(#Window_About)
  
EndProcedure

Procedure Exit()
  Quit = 1
EndProcedure

Procedure ScintillaCallback(Gadget, *scinotify.SCNotification)
EndProcedure

Procedure OpenEditorFile(FileName$="")  
  Protected *SciText, FileContents$, NewEditor
  ; Check if we have been passed a filename or not
  If FileName$ = ""
    FileName$ = OpenFileRequester("Open a file to edit...", "", "*.*", -1)
  EndIf

  FileContents$ = ""
  If ReadFile(#File_Open, FileName$)
    While Eof(#File_Open) = 0
      FileContents$ + ReadString(#File_Open) + Chr(13) + Chr(10)
    Wend
    
    CloseFile(#File_Open)
    
    *SciText = UTF8(FileContents$)
      
    If CountGadgetItems(#Panel_Main) = 1 And ScintillaSendMessage(#Editor_Main, #SCI_GETMODIFY) = 0
      ; Single tab, open in this tab
      NewEditor = #Editor_Main
      SetGadgetItemText(#Panel_Main, 0, FileName$)
    Else
      ; Open in new tab
      OpenGadgetList(#Panel_Main)
        AddGadgetItem(#Panel_Main, -1, FileName$)
        NewEditor = CreateScintillaEditor(CountGadgetItems(#Panel_Main))
      CloseGadgetList()
      
      SetGadgetItemText(#Panel_Main, -1, FileName$)
      SetGadgetState(#Panel_Main, CountGadgetItems(#Panel_Main) - 1)
    EndIf
    
    ScintillaSendMessage(NewEditor, #SCI_SETTEXT, 0, *SciText)
    FreeMemory(*SciText)
  Else
    MessageRequester("Error", "Could not open file: " + FileName$)
  EndIf
EndProcedure

Procedure OpenEditorFolder()
  Protected PathName$
  
  PathName$ = PathRequester("Open a folder...", GetCurrentDirectory())
  
  HideGadget(#Tree_Files, 0)
  SetGadgetText(#Tree_Files, PathName$)
EndProcedure

;- Entry Point / Hook
Inits()

;- Repeat / Event Loop
Repeat
  UpdateStatusBar()
  
  Event = WaitWindowEvent()
  Select Event()
    Case #PB_Event_CloseWindow : Exit()
    Case #PB_Event_Menu
      Select EventMenu()
        Case #Menu_File_Exit : Exit()
        Case #Menu_File_Open : OpenEditorFile()
        Case #Menu_File_OpenFolder : OpenEditorFolder()
        Case #Menu_File_CloseTab : CloseTab(GetGadgetState(#Panel_Main))
          
        Case #Menu_Help_About : About()
      EndSelect
    Case #PB_Event_Gadget
      Select EventGadget()
        Case #Button_About_Close : AboutClose()
        Case #Tree_Files
          Select EventType()
            Case #PB_EventType_LeftDoubleClick 
              If GetGadgetState(#Tree_Files) = 1 : OpenEditorFile(GetGadgetText(#Tree_Files)) : EndIf
          EndSelect
      EndSelect
  EndSelect
Until Quit = 1
End
; IDE Options = PureBasic 5.72 (Windows - x64)
; CursorPosition = 198
; FirstLine = 190
; Folding = -8
; EnableXP