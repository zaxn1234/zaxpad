# ZaxPad

## About The Project

Welcome! In this repo you will find the code to build your own ZaxPad!
ZaxPad is a custom IDE written for its developer.

## Built With

* [PureBasic](https://purebasic.com)
* [Scintilla](https://scintilla.org)

## Getting Started

This is an example of how you may give instructions on setting up your project locally.
To get a local copy up and running follow these simple example steps.

### Prerequisites 
This is only necessary if you wish to compile from source, otherwise go to `Installation (From ZIP)`

- Windows 10 (Linux/MacOS ports will be coming in the future don't worry)
- PureBasic v5.73 LTS x64 (Other versions _may_ work, but YMMV!)

### Installation (From source)

- Download the repo
- Open ZaxPad.pb in PureBasic (tested on v5.73 LTS x64)
- Compile the executable

### Installation (From ZIP)

- Download ZIP of the release version you'd like
- Extract ZIP to installation location
 - (So long as this folder has read/write permission for your user account, you should be good regardless of where it is!)
- Run `ZaxPad.exe`
- Code to your heart's desire!!

## Acknowledgements & Licenses
### Scintilla

License for Scintilla and SciTE

Copyright 1998-2003 by Neil Hodgson <neilh@scintilla.org>

All Rights Reserved 

Permission to use, copy, modify, and distribute this software and its 
documentation for any purpose and without fee is hereby granted, 
provided that the above copyright notice appear in all copies and that 
both that copyright notice and this permission notice appear in 
supporting documentation. 

NEIL HODGSON DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS 
SOFTWARE, INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY 
AND FITNESS, IN NO EVENT SHALL NEIL HODGSON BE LIABLE FOR ANY 
SPECIAL, INDIRECT OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES 
WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, 
WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER 
TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE 
OR PERFORMANCE OF THIS SOFTWARE. 


